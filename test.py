import gensim
import pickle
from toolz.sandbox import unzip
def main():
    ce = lambda a,b: a+b
    p = [i for i in range(10)]
    q = [10-i for i in range(10)]
    print(ce(p,q))
if __name__ == '__main__':
    main()
