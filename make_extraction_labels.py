# """produce the dataset with (psudo) extraction label"""
import os
import sys
from os.path import exists, join
import json
from time import time
from datetime import timedelta
import multiprocessing as mp

from cytoolz import curry, compose
from functools import partial
# from utils import count_data
import rouge
import nltk


try:
    DATA_DIR = os.environ['DATA']
except KeyError:
    print('please use environment variable to specify data directories')


def _split_words(texts):
    return map(lambda t: t.split(), texts)


def get_extract_label(art_sents, abs_sents):
    """ greedily match summary sentences to article sentences"""
    extracted = []
    scores = []
    indices = list(range(len(art_sents)))
    evaluator = rouge.Rouge(metrics=['rouge-l'])
    for abst in abs_sents:	
        rouges=[]
        for art in art_sents:
        	roug = evaluator.get_scores(art, abst)
        	rouges.append(roug['rouge-l']['r'])
        ext = max(indices, key=lambda i: rouges[i])
        indices.remove(ext)
        extracted.append(ext)
        scores.append(rouges[ext])
        if not indices:
        	break
    return extracted, scores
def rect(l,b):
	area= l + str(b)
	data_dir = join(DATA_DIR, l)
	with open(join(data_dir, '{}.json'.format(b))) as f:
		data = json.loads(f.read())
	tokenize = compose(list, _split_words)
	art_sents = tokenize(data['article'])
	abs_sents = tokenize(data['abstract'])
	if art_sents and abs_sents:
		extracted, scores = get_extract_label(art_sents, abs_sents)
	else:
		extracted, scores = [], []
	data['extracted'] = extracted
	data['score'] = scores
	# with open(join(data_dir, '{}.json'.format(b)), 'w') as f:
	# 	json.dump(data, f, indent=4)
# @curry
def pocess(split, i):
    data_dir = join(DATA_DIR, split)
    with open(join(data_dir, '{}.json'.format(i))) as f:
        data = json.loads(f.read())
    art_sents = data['article']
    abs_sents = data['abstract']
    if art_sents and abs_sents: # some data contains empty article/abstract
        extracted, scores = get_extract_label(art_sents, abs_sents)
    else:
        extracted, scores = [], []
    data['extracted'] = extracted
    data['score'] = scores
    with open(join(data_dir, '{}.json'.format(i)), 'w') as f:
        json.dump(data, f, indent=4)

def label_mp(split):
    """ process the data split with multi-processing"""
    start = time()
    print('start processing {} split...'.format(split))
    data_dir = join(DATA_DIR, split)
    n_data = len(os.listdir(data_dir))
    lis = [i for i in range(n_data)]
    func = partial(pocess, split)
    pool = mp.Pool()
    list(pool.imap_unordered(func, lis, chunksize=1024))
	# list(pool.imap_unordered(func,lis,chunksize=10000))
    # func = partial(process, split)
    # with mp.Pool() as pool:
    #     list(pool.imap_unordered(func, lis, chunksize=1024))
    
    print('finished in {}'.format(timedelta(seconds=time()-start)))
    # print(lis[0:5])
def label(split):
    start = time()
    print('start processing {} split...'.format(split))
    data_dir = join(DATA_DIR, split)
    n_data = len(os.listdir(data_dir))
    for i in range(n_data):
        print('processing {}/{} ({:.2f}%%)\r'.format(i, n_data, 100*i/n_data),
              end='')
        with open(join(data_dir, '{}.json'.format(i))) as f:
            data = json.loads(f.read())
        tokenize = compose(list, _split_words)
        art_sents = tokenize(data['article'])
        abs_sents = tokenize(data['abstract'])
        extracted, scores = get_extract_label(art_sents, abs_sents)
        data['extracted'] = extracted
        data['score'] = scores
        with open(join(data_dir, '{}.json'.format(i)), 'w') as f:
            json.dump(data, f, indent=4)
    print('finished in {}'.format(timedelta(seconds=time()-start)))


def main():
    for split in ['val', 'train']:  # no need of extraction label when testing
        label_mp(split)

if __name__ == '__main__':
    main()
