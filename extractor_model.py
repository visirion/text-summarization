import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init



INI = 1e-2
def prob_normalize(score, mask):
    score = score.masked_fill(mask == 0, -1e18)
    norm_score = F.softmax(score, dim=-1)
    return norm_score
def reorder_lstm_states(lstm_states, order):

    assert isinstance(lstm_states, tuple)
    assert len(lstm_states) == 2
    assert lstm_states[0].size() == lstm_states[1].size()
    assert len(order) == lstm_states[0].size()[1]

    order = torch.LongTensor(order).to(lstm_states[0].device)
    sorted_states = (lstm_states[0].index_select(index=order, dim=1),lstm_states[1].index_select(index=order, dim=1))

    return sorted_states
def len_mask(lens, device):
    """ users are resposible for shaping
    Return: tensor_type [B, T]
    """
    max_len = max(lens)
    batch_size = len(lens)
    mask = torch.ByteTensor(batch_size, max_len).to(device)
    mask.fill_(0)
    for i, l in enumerate(lens):
        mask[i, :l].fill_(1)
    return mask

def sequence_mean(sequence, seq_lens, dim=1):
    if seq_lens:
        assert sequence.size(0) == len(seq_lens)
        sum_=torch.sum(sequence, dim=dim, keepdim=False)
        mean = torch.stack([s/l for s, l in zip(sum_, seq_lens)], dim=0)
    else:
        mean = torch.mean(sequence, dim=dim, keepdim=False)
    return mean

def reorder_sequence(sequence_emb, order, batch_first=False):
    batch_dim = 0 if batch_first else 1
    assert len(order) == sequence_emb.size()[batch_dim]
    order = torch.LongTensor(order).to(sequence_emb.device)
    sorted_= sequence_emb.index_select(index=order, dim=batch_dim)
    return sorted_

def init_lstm_states(lstm, batch_size, device):
    n_layer = lstm.num_layers*(2 if lstm.bidirectional else 1)
    n_hidden = lstm.hidden_size
    states = (torch.zeros(n_layer, batch_size, n_hidden).to(device),torch.zeros(n_layer, batch_size, n_hidden).to(device))
    return states

def lstm_encoder(sequence, lstm, seq_lens=None, init_states=None, embedding=None):
    batch_size = sequence.size(0)
    if not lstm.batch_first:
        sequence = sequence.transpose(0,1)
        emb_sequence = (embedding(sequence) if embedding is not None else sequence)
    if seq_lens:
        assert batch_size == len(seq_lens)
        sort_ind = sorted(range(len(seq_lens)), key=lambda i: seq_lens[i], reverse=True)
        emb_sequence = reorder_sequence(emb_sequence, sort_ind, lstm.batch_first)
    if init_states is None:
        device = sequence.device
        init_states = init_lstm_states(lstm, batch_size, device)
    else:
        init_states = (init_states[0].contiguous(), init_states[1].contiguous())
    if seq_lens:
        # print("...........................................................", emb_sequence)
        # print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",seq_lens)
        packed_seq = nn.utils.rnn.pack_padded_sequence(emb_sequence, seq_lens, enforce_sorted=False)
        packed_out, final_states = lstm(packed_seq, init_states)
        lstm_out, _ = nn.utils.rnn.pad_packed_sequence(packed_out)
        back_map = {ind: i for i, ind in enumerate(sort_ind)}
        reorder_ind = [back_map[i] for i in range(len(seq_lens))]
        lstm_out = reorder_sequence(lstm_out, reorder_ind, lstm.batch_first)
        final_states = reorder_lstm_states(final_states, reorder_ind)
    else:
        lstm_out, final_states = lstm(emb_sequence, init_states)
    return lstm_out, final_states








class Convolve(nn.Module):
    def __init__(self, vsize, emb_dim, conv_hidden, dropout):
        super().__init__()
        self._embedding = nn.Embedding(vsize, emb_dim, padding_idx=0)
        self._convs = nn.ModuleList([nn.Conv1d(emb_dim, conv_hidden, i) for i in range(3,6)])
        self._dropout = dropout

    def forward(self, input_):
        emb_input = self._embedding(input_)
        conv = F.dropout(emb_input.transpose(1,2), self._dropout, training= self.training)
        output = torch.cat([F.relu(conv_p(conv)).max(dim=2)[0] for conv_p in self._convs], dim=1)
        return output
    def set_embedding(self, embedding):
        assert self._embedding.weight.size() == embedding.size()
        self._embedding.weight.data.copy_(embedding)

class LSTMEncoder(nn.Module):
    def __init__(self, input_dim, n_hidden, n_layer, dropout, bidirectional):
        super().__init__()
        self._init_h = nn.Parameter(torch.Tensor(n_layer*(2 if bidirectional else 1), n_hidden))
        self._init_c = nn.Parameter(torch.Tensor(n_layer*(2 if bidirectional else 1), n_hidden))
        init.uniform_(self._init_h, -INI, INI)
        init.uniform_(self._init_c, -INI, INI)
        self._lstm = nn.LSTM(input_dim, n_hidden, n_layer, dropout=dropout, bidirectional=bidirectional)
    def forward(self, input_, in_lens=None):
        size = (self._init_h.size(0), input_.size(0), self._init_h.size(1))
        init_states = (self._init_h.unsqueeze(1).expand(*size),self._init_c.unsqueeze(1).expand(*size))
        lstm_out, _ = lstm_encoder(input_, self._lstm, in_lens, init_states)
        return lstm_out.transpose(0,1)
    @property
    def input_size(self):
        return self._lstm.input_size

    @property
    def hidden_size(self):
        return self._lstm.hidden_size

    @property
    def num_layers(self):
        return self._lstm.num_layers

    @property
    def bidirectional(self):
        return self._lstm.bidirectional


class Extract_ff(nn.Module):
    def __init__(self, vsize, emb_dim, conv_hidden, lstm_hidden, lstm_layer, bidirectional, dropout=0.0):
        super().__init__()
        self._sent_enc = Convolve(vsize, emb_dim, conv_hidden, dropout)
        self._art_enc = LSTMEncoder(3*conv_hidden, lstm_hidden, lstm_layer, dropout=dropout, bidirectional = bidirectional)
        lstm_out_dim = lstm_hidden * (2 if bidirectional else 1)
        self._sent_linear = nn.Linear(lstm_out_dim, 1)
        self._art_linear = nn.Linear(lstm_out_dim, lstm_out_dim)

    def forward(self, article_sents, sent_nums):
        encoded_sent, encoded_art = self._encode(article_sents, sent_nums)
        saliency = torch.matmul(encoded_sent, encoded_art.unsqueeze(2))
        saliency = torch.cat([s[:n] for s,n in zip(saliency, sent_nums)], dim=0)
        cont = self._sent_linear(torch.cat([s[:n] for s,n in zip(encoded_sent, sent_nums)], dim=0))
        logits = (content + saliency).squeeze(1)
        return logits


    def _encode(article_sents, sent_nums):
        if sent_nums is None:
            enc_sent = self._sent_enc(article_sents[0]).unsqueeze(0)
        else:
            max_n = max(sent_nums)
            enc_sents = [self._sent_enc(sent) for sent in article_sents]
            def zero(n, device):
                z = torch.zeros((n, self._art_enc.input_size)).to(device)
                return z
            lis = []
            for s,n in zip(enc_sents, sent_nums):
                if(n==max_n):
                    lis.append(s)
                else:
                    lis.append(torch.cat([s, zero(max_n- n, s.device)], dim=0))
            enc_sent = torch.stack(lis, dim=0)
        lstm_out = self._art_enc(enc_sent, sent_nums)
        enc_art = torch.tanh(self._art_linear(sequence_mean(lstm_out, sent_nums, dim=1)))
        return lstm_out, enc_art
    def extract(self, article_sents, sent_nums=None, k=4):
        #extract top-k scored sentences from article
        enc_sent, enc_art = self._encode(article_sents, sent_nums)
        saliency = torch.matmul(enc_sent, enc_art.unsqueeze(2))
        content = self._sent_linear(enc_sent)
        logit = (content + saliency).squeeze(2)
        if sent_nums is None:  # test-time extract only
            assert len(article_sents) == 1
            n_sent = logit.size(1)
            extracted = logit[0].topk(k if k < n_sent else n_sent, sorted=False)[1].tolist()
        else:
            extracted = [l[:n].topk(k if k < n else n)[1].tolist() for n, l in zip(sent_nums, logit)]
        return extracted
class StackedLSTMCells(nn.Module):
    def __init__(self, cells, dropout=0.0):
        super().__init__()
        self._cells=nn.ModuleList(cells)
        self._dropout=dropout
    def forward(self, input_, state):
        hs=[]
        cs=[]
        for i, cell in enumerate(self._cells):
            s = (state[0][i, :, :], state[1][i, :, :])
            h, c = cell(input_, s)
            hs.append(h)
            cs.append(c)
            input_ = F.dropout(h, p=self._dropout, training=self.training)
        new_h = torch.stack(hs, dim=0)
        new_c = torch.stack(cs, dim=0)

        return new_h, new_c
class MultiLayerLSTMCells(StackedLSTMCells):
    def __init__(self, input_size, hidden_size, num_layers, bias=True, dropout=0.0):
        cells=[]
        cells.append(nn.LSTMCell(input_size, hidden_size, bias))
        for _ in range(num_layers-1):
            cells.append(nn.LSTMCell(hidden_size, hidden_size, bias))
        super().__init__(cells, dropout)
class LSTMPointerNet(nn.Module):
    def __init__(self, input_dim, n_hidden, n_layer, dropout, n_hop):
        super().__init__()
        self._init_h = nn.Parameter(torch.Tensor(n_layer, n_hidden))
        self._init_c = nn.Parameter(torch.Tensor(n_layer, n_hidden))
        self._init_i = nn.Parameter(torch.Tensor(input_dim))
        nn.init.uniform_(self._init_h, -INI, INI)
        nn.init.uniform_(self._init_c, -INI, INI)
        nn.init.uniform_    (self._init_i, -0.1, 0.1)
        self._lstm = nn.LSTM(input_dim, n_hidden, n_layer, bidirectional=False, dropout=dropout)
        self._lstm_cell=None

        #attention parameters
        self._attn_wm = nn.Parameter(torch.Tensor(input_dim, n_hidden))
        self._attn_wq = nn.Parameter(torch.Tensor(n_hidden, n_hidden))
        self._attn_v = nn.Parameter(torch.Tensor(n_hidden))
        init.xavier_normal_(self._attn_wm)
        init.xavier_normal_(self._attn_wq)
        init.uniform_(self._attn_v, -INI, INI)

        #hop parameters

        self._hop_wm = nn.Parameter(torch.Tensor(input_dim, n_hidden))
        self._hop_wq = nn.Parameter(torch.Tensor(n_hidden, n_hidden))
        self._hop_v = nn.Parameter(torch.Tensor(n_hidden))
        init.xavier_normal_(self._hop_wm)
        init.xavier_normal_(self._hop_wq)
        init.uniform_(self._hop_v, -INI, INI)
        self._n_hop = n_hop
    def forward(self, atten_mem, mem_size, lstm_in):
        attn_feat, hop_feat, lstm_states, init_i = self._prepare(atten_mem)
        lstm_in = torch.cat([init_i, lstm_in], dim=1).transpose(0,1)
        query, final_states = self._lstm(lstm_in, lstm_states)
        query = query.transpose(0,1)
        for _ in range(self._n_hop):
        	query = LSTMPointerNet.attention(hop_feat, query, self._hop_v, self._hop_wq, mem_size)
        output = LSTMPointerNet.attention_score(attn_feat, query, self._attn_v, self._attn_wq)
        return output

    def _prepare(self, attn_mem):
        batch_size = attn_mem.size(0)
        num_sen, dim = self._init_h.size()
        size = (num_sen, batch_size, dim)
        lstm_states = (self._init_h.unsqueeze(1).expand(*size),self._init_c.unsqueeze(1).expand(*size))
        d = self._init_i.size(0)
        init_i = self._init_i.unsqueeze(0).unsqueeze(1).expand(batch_size,1,d)
        attn_feat = torch.matmul(attn_mem, self._attn_wm.unsqueeze(0))
        hop_feat = torch.matmul(attn_mem, self._hop_wm.unsqueeze(0))
        #hop_feat is of shape (batch_sz, num_sen, n_hidden)
        return attn_feat, hop_feat, lstm_states, init_i

    def attention_score(attention, query, v, w):
        sum_ = attention.unsqueeze(1) + torch.matmul(query, w.unsqueeze(0)).unsqueeze(2)
        score = torch.matmul(torch.tanh(sum_), v.unsqueeze(0).unsqueeze(1).unsqueeze(3)).squeeze(3)
        return score

    def attention(attention, query, v, w, mem_sizes):

        score = LSTMPointerNet.attention_score(attention, query, v, w)
        if mem_sizes is None:
            norm_score = F.softmax(score, dim=-1)
        else:
            mask = len_mask(mem_sizes, score.device).unsqueeze(-2)
            norm_score = prob_normalize(score, mask)
        output = torch.matmul(norm_score, attention)
        return output
    def extract(self, attn_mem, mem_sizes, k):
        #batch_size=1
        attn_feat, hop_feat, lstm_states, lstm_in = self._prepare(attn_mem)
        lstm_in = lstm_in.squeeze(1)
        if self._lstm_cell is None:
            self._lstm_cell = MultiLayerLSTMCells.convert(self._lstm).to(attn_mem.device)
        extracts = []
        for _ in range(k):
            h, c = self._lstm_cell(lstm_in, lstm_states)
            query = h[-1]
            for _ in range(self._n_hop):
                query = LSTMPointerNet.attention(hop_feat, query, self._hop_v, self._hop_wq, mem_sizes)
            score = LSTMPointerNet.attention_score(attn_feat, query, self._attn_v, self._attn_wq)
            score = score.squeeze()
            for e in extracts:
                score[e] = -1e6
            ext = score.max(dim=0)[1].item()
            extracts.append(ext)
            lstm_states = (h, c)
            lstm_in = attn_mem[:, ext, :]
        return extracts


class Extract_ptr(nn.Module):
    def __init__(self, vsize, emb_dim, conv_hidden, lstm_hidden, lstm_layer, bidirectional, n_hop = 1, dropout=0.0):
        # print("heeeee")
        super().__init__()
        self._sent_enc = Convolve(vsize, emb_dim, conv_hidden, dropout)
        self._art_enc = LSTMEncoder(3*conv_hidden, lstm_hidden, lstm_layer, dropout=dropout, bidirectional=bidirectional)
        enc_out_dim = lstm_hidden * (2 if bidirectional else 1)
        self._extractor = LSTMPointerNet(enc_out_dim, lstm_hidden, lstm_layer, dropout, n_hop)
    def forward(self, article_sents, sent_nums, target):
        enc_out  = self._encode(article_sents, sent_nums)
        batch_size, num_sen = target.size()
        lstm_out_d = enc_out.size(2)
        #now ptr_in is in order of target
        ptr_in = torch.gather(enc_out, dim = 1, index=target.unsqueeze(2).expand(batch_size, num_sen, lstm_out_d))
        output = self._extractor(enc_out, sent_nums, ptr_in)
        return output
    def _encode(self, article_sents, sent_nums):
        if sent_nums is None:
            enc_sent = self._sent_enc(article_sents[0]).unsqueeze(0)
        else:
            max_n = max(sent_nums)
            enc_sents = [self._sent_enc(art) for art in article_sents]
            def zero(n, device):
                # eta = self._art_enc.input_size
                # print("eta theta = ", [int(eta),type(eta)])
                zz = torch.zeros(n, self._art_enc.input_size)
                zz = zz.to(device)
                return zz
            lis = []
            for s,n in zip(enc_sents, sent_nums):
                if(n==max_n):
                    lis.append(s)
                else:
                    lis.append(torch.cat([s, zero(max_n- n, s.device)], dim=0))
            enc_sent = torch.stack(lis, dim=0)
        lstm_out = self._art_enc(enc_sent, sent_nums)
        return lstm_out
    def extract(self, article_sents, sent_nums=None, k=4):
        enc_out = self._encode(article_sents, sent_nums)
        output = self._extractor.extract(enc_out, sent_nums, k)
        return output
    def set_embedding(self, embedding):
        self._sent_enc.set_embedding(embedding)
