import argparse
import time
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.multiprocessing as mp
from torch import optim
from torch.nn.utils import clip_grad_norm_
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader, Dataset
import argparse
import json
import re
import os
from os.path import join, exists
from cytoolz import compose, concat, curry, reduce
import pickle as pkl
from utility import *
from functools import partial
from toolz.sandbox import unzip
from collections import defaultdict
from data import *
from extractor_model import *
import tensorboardX
import random
import math
BUCKET_SIZE = 6400
PAD = 0
UNK = 1
START = 2
END = 3
DATA_DIR=None
'''
THE FLOW:

First data is segregated into buckets of size bucketsize. Then it is further divided into batches of size args.batch_size.
After that data is preprocessed and converted into tokens. Now each word is converted into its id. Further it is batchified where it
is padded. Then data is send for training, where each batch has tensors of shape (num_sen,num_words)






'''
'''Classes'''
def get_basic_grad_fn(net, clip_grad, max_grad=1e2):
    def f():
        grad_norm = clip_grad_norm_(
            [p for p in net.parameters() if p.requires_grad], clip_grad)
        grad_norm = grad_norm.item()
        if max_grad is not None and grad_norm >= max_grad:
            print('WARNING: Exploding Gradients {:.2f}'.format(grad_norm))
            grad_norm = max_grad
        grad_log = {}
        grad_log['grad_norm'] = grad_norm
        return grad_log
    return f
def _batch2q(loader, prepro, q, single_run=True):
    epoch = 0
    while True:
        for batch in loader:
            q.put(prepro(batch))
        if single_run:
            break
        epoch += 1
        q.put(epoch)
    q.put(None)

class BucketedGenerator(object):
    def __init__(self, data_loader, preprocess, sort_key, batchify, single_run=True, fork=True):
        self._loader = data_loader
        self._preprocess = preprocess
        self._sort_key = sort_key
        self._batchify = batchify
        self._single_run = single_run
        if fork:
            ctx = mp.get_context('forkserver')
            self._queue=ctx.Queue()
        else:
            self._queue=None
        self._process = None
    def __call__(self, batch_size):
        def get_batches(hyper_batch):
            indexes = list(range(0, len(hyper_batch), batch_size))
            if not self._single_run:
                random.shuffle(hyper_batch)
                random.shuffle(indexes)
            hyper_batch.sort(key=self._sort_key)
            for i in indexes:
                batch = self._batchify(hyper_batch[i:i+batch_size])
                yield batch
        if self._queue is not None:
            ctx = mp.get_context('forkserver')
            self._process = ctx.Process(target = _batch2q, args=(self._loader, self._preprocess, self._queue, self._single_run))
            self._process.start()
            while True:
                d = self._queue.get()
                if d is None:
                    break
                if isinstance(d, int):
                    print('\nepoch {} done'.format(d))
                    continue
                yield from get_batches(d)
            self._process.join()
        else:
            i=0
            while True:
                for batch in self._loader:
                    yield from get_batches(self._preprocess(batch))
                if self._single_run==True:
                    break
                i+=1
                print('\nepoch {} done'.format(d))
    def terminate(self):
        if self._process is not None:
            self._process.terminate()
            self._process.join()

class BasicPipeline(object):
    def __init__(self, name, net,train_batcher, val_batcher, batch_size,val_fn, criterion, optim, grad_fn=None):
        self.name = name
        self._net = net
        self._train_batcher = train_batcher
        self._val_batcher = val_batcher
        self._criterion = criterion
        self._opt = optim
        # grad_fn is calleble without input args that modifyies gradient
        # it should return a dictionary of logging values
        self._grad_fn = grad_fn
        self._val_fn = val_fn

        self._n_epoch = 0  # epoch not very useful?
        self._batch_size = batch_size
        self._batches = self.batches()

    def batches(self):
        while True:
            for fw_args, bw_args in self._train_batcher(self._batch_size):
                yield fw_args, bw_args
            self._n_epoch += 1

    def get_loss_args(self, net_out, bw_args):
        if isinstance(net_out, tuple):
            loss_args = net_out + bw_args
        else:
            loss_args = (net_out, ) + bw_args
        return loss_args

    def train_step(self):
        # forward pass of model
        self._net.train()
        fw_args, bw_args = next(self._batches)
        net_out = self._net(*fw_args)

        # get logs and output for logging, backward
        log_dict = {}
        loss_args = self.get_loss_args(net_out, bw_args)

        # backward and update ( and optional gradient monitoring )
        loss = self._criterion(*loss_args).mean()
        loss.backward()
        log_dict['loss'] = loss.item()
        if self._grad_fn is not None:
            log_dict.update(self._grad_fn())
        self._opt.step()
        self._net.zero_grad()

        return log_dict

    def validate(self):
        return self._val_fn(self._val_batcher(self._batch_size))

    def checkpoint(self, save_path, step, val_metric=None):
        save_dict = {}
        if val_metric is not None:
            name = 'ckpt-{:6f}-{}'.format(val_metric, step)
            save_dict['val_metric'] = val_metric
        else:
            name = 'ckpt-{}'.format(step)

        save_dict['state_dict'] = self._net.state_dict()
        save_dict['optimizer'] = self._opt.state_dict()
        torch.save(save_dict, join(save_path, name))

    def terminate(self):
        self._train_batcher.terminate()
        self._val_batcher.terminate()


class BasicTrainer(object):
    """ Basic trainer with minimal function and early stopping"""
    def __init__(self, pipeline, save_dir, ckpt_freq, patience,
                 scheduler=None, val_mode='loss'):
        assert isinstance(pipeline, BasicPipeline)
        assert val_mode in ['loss', 'score']
        self._pipeline = pipeline
        self._save_dir = save_dir
        self._logger = tensorboardX.SummaryWriter(join(save_dir, 'log'))
        # print("iejhbkjnlkcmnjbhjvgjhbj")
        os.makedirs(join(save_dir, 'ckpt'))

        self._ckpt_freq = ckpt_freq
        self._patience = patience
        self._sched = scheduler
        self._val_mode = val_mode

        self._step = 0
        self._running_loss = None
        # state vars for earlythunderthunder stopping
        self._current_p = 0
        self._best_val = None

    def log(self, log_dict):
        loss = log_dict['loss'] if 'loss' in log_dict else log_dict['reward']
        if self._running_loss is not None:
            self._running_loss = 0.99*self._running_loss + 0.01*loss
        else:
            self._running_loss = loss
        print('train step: {}, {}: {:.4f}\r'.format(
            self._step,
            'loss' if 'loss' in log_dict else 'reward',
            self._running_loss), end='')
        for key, value in log_dict.items():
            self._logger.add_scalar(
                '{}_{}'.format(key, self._pipeline.name), value, self._step)

    def validate(self):
        val_log = self._pipeline.validate()
        for key, value in val_log.items():
            self._logger.add_scalar('val_{}_{}'.format(key, self._pipeline.name),value, self._step)
        if 'reward' in val_log:
            val_metric = val_log['reward']
        else:
            val_metric = (val_log['loss'] if self._val_mode == 'loss'else val_log['score'])
        return val_metric

    def checkpoint(self):
        val_metric = self.validate()
        self._pipeline.checkpoint(
            join(self._save_dir, 'ckpt'), self._step, val_metric)
        if isinstance(self._sched, ReduceLROnPlateau):
            self._sched.step(val_metric)
        else:
            self._sched.step()
        stop = self.check_stop(val_metric)
        return stop

    def check_stop(self, val_metric):
        if self._best_val is None:
            self._best_val = val_metric
        elif ((val_metric < self._best_val and self._val_mode == 'loss')
              or (val_metric > self._best_val and self._val_mode == 'score')):
            self._current_p = 0
            self._best_val = val_metric
        else:
            self._current_p += 1
        return self._current_p >= self._patience

    def train(self):
        try:
            start = time.time()
            print('Start training')
            while True:
                log_dict = self._pipeline.train_step()
                self._step += 1
                self.log(log_dict)

                if self._step % self._ckpt_freq == 0:
                    stop = self.checkpoint()
                    if stop:
                        break
            print('Training finised in ', timedelta(seconds=time.time()-start))
        finally:
            self._pipeline.terminate()
'''functions'''
@curry
def tokenize(max_sent_len, doc):
    return [line.lower().split()[:max_sent_len] for line in doc]
def sequence_loss(logits, targets, xent_fn=None, pad_idx=0):
    assert logits.size()[:-1] == targets.size()

    mask= targets!=pad_idx
    target = targets.masked_select(mask)
    logit = logits.masked_select(mask.unsqueeze(2).expand_as(logits)).contiguous().view(-1, logits.size(-1))
    if xent_fn:
        print('logit========', logit)
        loss = xent_fn(logit, target)
    else:
        loss = F.cross_entropy(logit, target)
    assert (not math.isnan(loss.mean().item()) and not math.isinf(loss.mean().item()))
    return loss
@curry
def val_step(loss_step, fw_args, loss_args):
    loss = loss_step(fw_args, loss_args)
    return loss.size(0), loss.sum().item()
@curry
def compute_loss(net, criterion, fw_args, loss_args):
    loss = criterion(*((net(*fw_args),) + loss_args))
    return loss

@curry
def basic_validate(net, criterion, val_batches):
    print('running validation ... ', end='')
    net.eval()
    start = time()
    with torch.no_grad():
        validate_fn = val_step(compute_loss(net, criterion))
        n_data, tot_loss = reduce(lambda a, b: (a[0]+b[0], a[1]+b[1]),starmap(validate_fn, val_batches),(0, 0))
    val_loss = tot_loss / n_data
    print('validation finished in {}'.format(timedelta(seconds=int(time()-start))))
    print('validation loss: {:.4f} ... '.format(val_loss))
    return {'loss': val_loss}
@curry
def coll_fn_extract(data):
    def is_good(d):
        sources, extracts =d
        return sources and extracts
    batch = list(filter(is_good, data))
    assert all(map(is_good, batch))
    return batch
@curry
def preprocess_func(max_sent_len, max_sent, batch):
    def preprocess_func_one_summary(summary):
        source, extract = summary
        tokenized_sents= tokenize(max_sent_len, source)[:max_sent]
        cleaned_extracts = list(filter(lambda e: e<len(tokenized_sents),extract))
        return tokenized_sents, cleaned_extracts
    batch = list(map(preprocess_func_one_summary, batch))
    return batch
def convert2id(unk, word2id, sources):
    word2id = defaultdict(lambda: unk, word2id)
    return [[word2id[word] for word in sent] for sent in sources]
@curry
def pad_batch_tensorize(inputs, pad, cuda=True):
    tensor_type = torch.cuda.LongTensor if cuda else torch.LongTensor
    batch_size = len(inputs)
    max_len = max(len(ids) for ids in inputs)
    tensor_shape = (batch_size, max_len)
    tensor = tensor_type(*tensor_shape)
    tensor.fill_(pad)
    for i, ids in enumerate(inputs):
        tensor[i,:len(ids)] = tensor_type(ids)
    return tensor
@curry
def convert_ff(unk, word2id, batch):
    def convert_one_summary(summary):
        sources, extracts = summary
        id_sents = convert2id(unk, word2id, sources)
        binary_extracts = [0]*len(sources)
        for ext in extracts:
            binary_extracts[ext] = 1
        return id_sents, binary_extracts
    batch = list(map(convert_one_summary, batch))
    return batch
@curry
def convert_ptr(unk, word2id, batch):
    def convert_one_summary(summary):
        sources, extracts = summary
        id_sents = convert2id(unk, word2id, sources)
        return id_sents, extracts
    batch = list(map(convert_one_summary, batch))
    return batch
@curry
def batchify_ff(pad, data, cuda=True):
    sources, targets = tuple(map(list, unzip(data)))
    src_num = list(map(len, sources))
    sources = list(map(pad_batch_tensorize(pad=pad, cuda=cuda), sources))
    tensor_type = torch.cuda.FloatTensor if cuda else torch.FloatTensor
    target = tensor_type(list(concat(targets)))
    forward_args = (sources, src_num)
    loss_args = (target,)
    return forward_args,loss_args
@curry
def batchify_ptr(pad, data, cuda=True):
    sources, targets = tuple(map(list, unzip(data)))
    src_num = list(map(len, sources))
    sources = list(map(pad_batch_tensorize(pad=pad, cuda=cuda), sources))
    target = pad_batch_tensorize(targets, pad=-1, cuda=cuda)
    remove_last= lambda tgt: tgt[:-1]
    tar_in = pad_batch_tensorize(list(map(remove_last, targets)), pad=-0, cuda=cuda)
    forward_args = (sources, src_num, tar_in)
    loss_args = (target, )
    return forward_args, loss_args
def batch_t_n_v(net_type, word2id, cuda, debug):
    if(net_type!='ff' and net_type!='rnn'):
        return None, None
    def sort_key(sample):
        src_sents, _=sample
        return len(src_sents)
    if(net_type=='ff'):
        batchify_func = batchify_ff
        convert_func = convert_ff
    else:
        batchify_func = batchify_ptr
        convert_func = convert_ptr
    #first convert then apply batchify
    preprocess = preprocess_func(args.max_word, args.max_sent)

    batchify = compose(batchify_func(PAD, cuda=cuda), convert_func(UNK, word2id))
    train_data_loader = DataLoader(Extract_Data('train'), batch_size= BUCKET_SIZE, shuffle=not debug, num_workers=4 if cuda and not debug else 0, collate_fn=coll_fn_extract)
    batch_train = BucketedGenerator(train_data_loader, preprocess, sort_key, batchify,single_run=False, fork=not debug)

    val_data_loader=DataLoader(Extract_Data('val'), batch_size= BUCKET_SIZE, shuffle=not debug, num_workers=4 if cuda and not debug else 0, collate_fn=coll_fn_extract)
    batch_val= BucketedGenerator(val_data_loader, preprocess, sort_key, batchify, single_run=True, fork=not debug)

    return batch_train, batch_val
def config_model(net_type, vocab_sz, emb_dim, conv_hidden, lstm_hidden, lstm_layer, bidirectional):
    assert net_type in ['ff', 'rnn']
    # print('net_args =', type(net_type))
    net_args = {}
    net_args['vsize'] = vocab_sz
    net_args['emb_dim'] = emb_dim
    net_args['conv_hidden']=conv_hidden
    net_args['lstm_hidden']=lstm_hidden
    net_args['lstm_layer']=lstm_layer
    net_args['bidirectional'] = bidirectional
    # print('net_args =', **net_args)
    net = (Extract_ff(**net_args) if net_type == 'ff' else Extract_ptr(**net_args))
    return net, net_args

def get_basic_grad_func(net, clip_grad, max_grad=1e2):
    def f():
        grad_norm = clip_grad_norm_([p for p in net.parameters() if p.requires_grad], clip_grad)
        print("=========================\n",[grad_norm, type(grad_norm)])
        # grad_norm = /.item()
        if max_grad is not None and grad_norm >= max_grad:
            print('WARNING: Exploding Gradients {:.2f}'.format(grad_norm))
            grad_norm = max_grad
        grad_log = {}
        grad_log['grad_norm'] = grad_norm
        return grad_log
    return f


def config_train(net_type, opt, lr, clip_grad, lr_decay, batch_size):
    assert opt in ['adam'] #only for adam optimizer
    assert net_type in ['ff', 'rnn']
    opt_kwargs = {}
    opt_kwargs['lr']=lr
    train_params={}
    train_params['optimizer']      = (opt, opt_kwargs)
    train_params['clip_grad_norm'] = clip_grad
    train_params['batch_size']     = batch_size
    train_params['lr_decay']       = lr_decay
    if(net_type=='ff'):
        criterion = lambda logit, target: F.binary_cross_entropy_with_logits(logit, target, reduce=False)
    else:
        ce=lambda logit, target: F.cross_entropy(logit, target, reduce=False)
        def criterion(logits, targets):
            return sequence_loss(logits, targets, ce, pad_idx=-1)
    return criterion, train_params
try:
    DATA_DIR = os.environ['DATA']
except KeyError:
    print('baby export the DATA variable first')


def main(args):
    assert args.net_type in ['ff', 'rnn']
	#form word2id mapping using pickle file
    with open(join(DATA_DIR, 'vocab_cnt.pkl'), 'rb') as f:
        words = pkl.load(f)
    word2id = make_vocab(words, args.vsize)
    #the batchers for training and validation
    batch_train, batch_val = batch_t_n_v(args.net_type, word2id, args.cuda, args.debug)

    #Now, lets do something for model
    net, net_args = config_model(args.net_type, len(word2id), args.emb_dim, args.conv_hidden, args.lstm_hidden, args.lstm_layer, args.bi)

    embedding = None
    if args.w2v:
        embedding, OOV = make_embedding({i:w for w,i in word2id.items()},args.w2v)
        net.set_embedding(embedding)

    criterion, train_params = config_train(args.net_type, 'adam', args.lr, args.clip, args.decay, args.batch)
	#making directory to save model
    if not exists(args.path):
        os.makedirs(args.path)
    with open(join(args.path, 'vocab.pkl'), 'wb') as f:
        pkl.dump(word2id, f, pkl.HIGHEST_PROTOCOL)
    meta = {}
    meta['net']           = 'ml_{}_extractor'.format(args.net_type)
    meta['net_args']      = net_args
    meta['traing_params'] = train_params

    with open(join(args.path, 'meta.json'), 'w') as f:
        json.dump(meta, f, indent=4)

    #Preparing the Trainer
    val_func = basic_validate(net, criterion)
    grad_func = get_basic_grad_func(net, args.clip)
    optimizer = optim.Adam(net.parameters(), **train_params['optimizer'][1])
    scheduler= ReduceLROnPlateau(optimizer, 'min', verbose=True, factor= args.decay, min_lr=0, patience=args.lr_p)
    if args.cuda:
        net = net.cuda()
    pipeline = BasicPipeline(meta['net'], net, batch_train, batch_val, args.batch, val_func,criterion, optimizer, grad_func)
    trainer = BasicTrainer(pipeline, args.path, args.ckpt_freq, args.patience, scheduler)

    print('start training with the following hyper-parameters:')
    print(meta)
    trainer.train()
    #######################
    #######################


'''Providing all the hyper_parameters from terminal'''
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description= 'getting training parameteres from terminal')

    parser.add_argument('--path', required=True, help='root of the model')

    #model parameters
    parser.add_argument('--net_type', action='store', default='rnn', help='type of extractor model, either rnn or feed forward')
    parser.add_argument('--vsize', type=int, action='store', default=30000, help='vocabulary size')
    parser.add_argument('--emb_dim', type=int, action='store', default=128, help='the dimension of word embedding')
    parser.add_argument('--w2v', action='store', help='use pretrained word2vec embedding')
    parser.add_argument('--conv_hidden', type=int, action='store', default=100, help='the number of hidden units of Conv')
    parser.add_argument('--lstm_hidden', type=int, action='store', default=256, help='the number of hidden units of lSTM')
    parser.add_argument('--lstm_layer', type=int, action='store', default=1, help='the number of layers of LSTM Encoder')
    parser.add_argument('--no-bi', action='store_true', help='disable bidirectional LSTM encoder')
    parser.add_argument('--max_word', type=int, action='store', default=100, help='maximun words in a single article sentence')
    parser.add_argument('--max_sent', type=int, action='store', default=60, help='maximun sentences in an article')
    #training hyper_parameters
    parser.add_argument('--lr', type=float, action='store', default=1e-3, help='learning rate')
    parser.add_argument('--decay', type=float, action='store', default=0.5, help='learning rate decay ratio')
    parser.add_argument('--lr_p', type=int, action='store', default=0, help='patience for learning rate decay')
    parser.add_argument('--clip', type=float, action='store', default=2.0,help='gradient clipping')
    parser.add_argument('--batch', type=int, action='store', default=32, help='the training batch size')
    parser.add_argument('--ckpt_freq', type=int, action='store', default=3000,)
    parser.add_argument('--patience', type=int, action='store', default=5, help='patience for early stopping')
    parser.add_argument('--debug', action='store_true', help='run in debugging mode')
    parser.add_argument('--no-cuda', action='store_true', help='disable GPU training')
    args = parser.parse_args()
    # print("args= ", args)
    args.bi = not args.no_bi
    args.cuda = torch.cuda.is_available() and not args.no_cuda

    main(args)
