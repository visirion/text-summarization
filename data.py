import json
import re
import os
from os.path import join
from torch.utils.data import Dataset

try:
    DATA_DIR = os.environ['DATA']
except KeyError:
    print('baby export the DATA variable first')
def _count_data(path):
	matcher = re.compile(r'[0-9]+\.json')
	match = lambda name: bool(matcher.match(name))
	names = os.listdir(path)
	n_data = len(list(filter(match, names)))
	return n_data

class Summarize(CnnDmDataset):
    def __init__(self, split):
        assert split in ['val', 'test']
        super().__init__(split, DATASET_DIR)

    def __getitem__(self, i):
        js_data = super().__getitem__(i)
        art_sents = js_data['article']
        return art_sents


class CnnDailyMail(Dataset):
	def __init__(self, split:str, path: str) -> None:
		assert split in ['train', 'val', 'test']
		self._data_path=join(path, split)
		self._n_data=_count_data(self._data_path)
	def __len__(self):
		return self._n_data
	def __getitem__(self, i):
		with open(join(self._data_path, '{}.json'.format(i))) as f:
			js= json.loads(f.read())
		return js
class Extract_Data(CnnDailyMail):
    def __init__(self, split):
        super().__init__(split, DATA_DIR)
    def __getitem__(self,i):
        js = super().__getitem__(i)
        art_sents, extracts = js['article'], js['extracted']
        return art_sents, extracts
class MatchDataset(CnnDailyMail):
    def __init__(self, split):
        super().__init__(split, DATA_DIR)

    def __getitem__(self, i):
        js = super().__getitem__(i)
        art_sents, abs_sents, extracts = (
            js['article'], js['abstract'], js['extracted'])
        matched_arts = [art_sents[i] for i in extracts]
        return matched_arts, abs_sents[:len(extracts)]
