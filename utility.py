import re
import os
from os.path import basename
import gensim
import torch
import torch.nn as nn


''' Function to count number of files of name *{num}.json'''

def count_data(path):
	form = re.compile(r'[0-9]+\.json')
	match = lambda name: bool(form.match(name))
	names = os.listdir(path)
	num_= list(filter(match, names))
	return len(num_)

''' Now in our data we will index various not-so-main types of words

	PAD = 0
	UNK = 1
	START = 2
	END = 3

	And then form word2id mapping
'''
PAD = 0
UNK = 1
START = 2
END = 3

def make_vocab(words, len_vocab):
	word2id = {}
	word2id['<pad>'] = PAD
	word2id['<unk>'] = UNK
	word2id['<start>'] = START
	word2id['<end>'] = END
	for i, (w, _) in enumerate(words.most_common(len_vocab), 4):
		word2id[w]= i
	return word2id

'''Constructing the embedding matrix'''

def make_embedding(id2word, w2v_file):
	attrs = basename(w2v_file).split('.')
	w2v= gensim.models.KeyedVectors.load('/home/ancalagon/NLP/Summary/word2vec/word2vec.128d.226k.bin')
	len_vocab = len(id2word)
	# print('attrs===================',attrs)
	embedding_dim = int(attrs[-3][:-1])
	embedding = nn.Embedding(len_vocab, embedding_dim).weight
	OOVs =[]

	with torch.no_grad():
		for i in range(len(id2word)):
			if i == START:
				embedding[i, :]= torch.Tensor(w2v['<s>'])
			elif i == END:
				embedding[i, :]= torch.Tensor(w2v['<\s>'])
			elif id2word[i] in w2v:
				embedding[i,:]= torch.Tensor(w2v[id2word[i]])
			else:
				OOVs.append(i)
	return embedding, OOVs
