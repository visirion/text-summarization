import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn import init

INIT = 1e-2
def len_mask(lens, device):
    max_len = max(lens)
    batch_size = len(lens)
    mask = torch.ByteTensor(batch_size, max_len).to(device)
    mask.fill_(0)
    for i, l in enumerate(lens):
        mask[i, :l].fill_(1)
    return mask
def sequence_mean(sequence, seq_lens, dim=1):
    if seq_lens:
        assert sequence.size(0) == len(seq_lens)
        sum_=torch.sum(sequence, dim=dim, keepdim=False)
        mean = torch.stack([s/l for s, l in zip(sum_, seq_lens)], dim=0)
    else:
        mean = torch.mean(sequence, dim=dim, keepdim=False)
    return mean
def reorder_lstm_states(lstm_states, order):

    assert isinstance(lstm_states, tuple)
    assert len(lstm_states) == 2
    assert lstm_states[0].size() == lstm_states[1].size()
    assert len(order) == lstm_states[0].size()[1]

    order = torch.LongTensor(order).to(lstm_states[0].device)
    sorted_states = (lstm_states[0].index_select(index=order, dim=1),lstm_states[1].index_select(index=order, dim=1))

    return sorted_states
def init_lstm_states(lstm, batch_size, device):
    n_layer = lstm.num_layers*(2 if lstm.bidirectional else 1)
    n_hidden = lstm.hidden_size
    states = (torch.zeros(n_layer, batch_size, n_hidden).to(device),torch.zeros(n_layer, batch_size, n_hidden).to(device))
    return states

def reorder_sequence(sequence_emb, order, batch_first=False):
    batch_dim = 0 if batch_first else 1
    assert len(order) == sequence_emb.size()[batch_dim]
    order = torch.LongTensor(order).to(sequence_emb.device)
    sorted_= sequence_emb.index_select(index=order, dim=batch_dim)
    return sorted_

def lstm_encoder(sequence, lstm, seq_lens=None, init_states=None, embedding=None):
    batch_size = sequence.size(0)
    if not lstm.batch_first:
        sequence = sequence.transpose(0,1)
        emb_sequence = (embedding(sequence) if embedding is not None else sequence)
    if seq_lens:
        assert batch_size == len(seq_lens)
        sort_ind = sorted(range(len(seq_lens)), key=lambda i: seq_lens[i], reverse=True)
        emb_sequence = reorder_sequence(emb_sequence, sort_ind, lstm.batch_first)
    if init_states is None:
        device = sequence.device
        init_states = init_lstm_states(lstm, batch_size, device)
    else:
        init_states = (init_states[0].contiguous(), init_states[1].contiguous())
    if seq_lens:
        # print("...........................................................", emb_sequence)
        # print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++",seq_lens)
        packed_seq = nn.utils.rnn.pack_padded_sequence(emb_sequence, seq_lens, enforce_sorted=False)
        packed_out, final_states = lstm(packed_seq, init_states)
        lstm_out, _ = nn.utils.rnn.pad_packed_sequence(packed_out)
        back_map = {ind: i for i, ind in enumerate(sort_ind)}
        reorder_ind = [back_map[i] for i in range(len(seq_lens))]
        lstm_out = reorder_sequence(lstm_out, reorder_ind, lstm.batch_first)
        final_states = reorder_lstm_states(final_states, reorder_ind)
    else:
        lstm_out, final_states = lstm(emb_sequence, init_states)
    return lstm_out, final_states
#provides copy dimensions
INIT = 1e-2
def dot_attention_score(key, query):
    return query.matmul(key.transpose(1, 2))

def prob_normalize(score, mask):
    score = score.masked_fill(mask == 0, -1e18)
    norm_score = F.softmax(score, dim=-1)
    return norm_score

def attention_aggregate(value, score):
    output = score.matmul(value)
    return output

def step_attention(query, key, value, mem_mask=None):
    score = dot_attention_score(key, query.unsqueeze(-2))
    if mem_mask is None:
        norm_score = F.softmax(score, dim=-1)
    else:
        norm_score = prob_normalize(score, mem_mask)
    output = attention_aggregate(value, norm_score)
    return output.squeeze(-2), norm_score.squeeze(-2)

class _CopyLinear(nn.Module):
    def __init__(self, context_dim, state_dim, input_dim, bias=True):
        super().__init__()
        self._v_c = nn.Parameter(torch.Tensor(context_dim))
        self._v_s = nn.Parameter(torch.Tensor(state_dim))
        self._v_i = nn.Parameter(torch.Tensor(input_dim))
        init.uniform_(self._v_c, -INIT, INIT)
        init.uniform_(self._v_s, -INIT, INIT)
        init.uniform_(self._v_i, -INIT, INIT)
        #add bias parameter if bias is true
        if(bias):
            self._b = nn.Parameter(torch.zeros(1))
        else:
            self.register_parameter(None, '_b')
    def forward(self, context, state, input_):
        output = (torch.matmul(context, self._v_c.unsqueeze(1)) + torch.matmul(state, self._v_s.unsqueeze(1)) + torch.matmul(input_, self._v_i.unsqueeze(1)))
        if self._b is not  None:
            output = output + self._b.unsqueeze(0)
        return output
class StackedLSTMCells(nn.Module):
    def __init__(self, cells, dropout=0.0):
        super().__init__()
        self._cells=nn.ModuleList(cells)
        self._dropout=dropout
    def forward(self, input_, state):
        hs=[]
        cs=[]
        for i, cell in enumerate(self._cells):
            s = (state[0][i, :, :], state[1][i, :, :])
            h, c = cell(input_, s)
            hs.append(h)
            cs.append(c)
            input_ = F.dropout(h, p=self._dropout, training=self.training)
        new_h = torch.stack(hs, dim=0)
        new_c = torch.stack(cs, dim=0)

        return new_h, new_c
class MultiLayerLSTMCells(StackedLSTMCells):
    def __init__(self, input_size, hidden_size, num_layers, bias=True, dropout=0.0):
        cells=[]
        cells.append(nn.LSTMCell(input_size, hidden_size, bias))
        for _ in range(num_layers-1):
            cells.append(nn.LSTMCell(hidden_size, hidden_size, bias))
        super().__init__(cells, dropout)
class AttentionalLSTMDecoder(object):
    def __init__(self, embedding, lstm, attn_w, projection):
        super().__init__()
        self._embedding = embedding
        self._lstm = lstm
        self._attn_w = attn_w
        self._projection = projection
    def __call__(self, attention, init_states, target):
        max_len=target.size(1)
        states=init_states
        logits=[]
        for i in range(max_len):
            tok = target[:, i:i+1]
            logit, states, _ = self._step(tok, states, attention)
            logits.append(logit)
        logit = torch.stack(logits, dim=1)
        return logit

class CopyLSTMDecoder(AttentionalLSTMDecoder):
    def __init__(self, copy, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._copy = copy

    def _compute_copy_activation(self, context, state, input_, score):
        copy = self._copy(context, state, input_) * score
        return copy
    def _compute_gen_prob(self, dec_out, extend_vsize, eps=1e-6):
        logit = torch.mm(dec_out, self._embedding.weight.t())
        bsize, vsize = logit.size()
        if extend_vsize > vsize:
            ext_logit = torch.Tensor(bsize, extend_vsize-vsize).to(logit.device)
            ext_logit.fill_(eps)
            gen_logit = torch.cat([logit, ext_logit], dim=1)
        else:
            gen_logit = logit
        gen_prob = F.softmax(gen_logit, dim=-1)
        return gen_prob
    def _step(self, tok, states, attention):
        prev_states, prev_out = states
        lstm_in = torch.cat([self._embedding(tok).squeeze(1), prev_out], dim=1)
        states = self._lstm(lstm_in, prev_states)
        lstm_out = states[0][-1]
        query = torch.mm(lstm_out, self._attn_w)
        attention, attn_mask, extend_src, extend_vsize = attention
        context, score = step_attention(query, attention, attention, mem_mask=attn_mask)
        dec_out = self._projection(torch.cat([lstm_out, context], dim=1))
        gen_prob = self._compute_gen_prob(dec_out, extend_vsize)
        copy_prob = torch.sigmoid(self._copy(context, states[0][-1], lstm_in))
        lp = torch.log(((-copy_prob + 1) * gen_prob).scatter_add_(dim=1,index=extend_src.expand_as(score),src=score * copy_prob) + 1e-8)
        return lp, (states, dec_out), score
    def decode_step(self, tok, states, attention):
        logit, states, score = self._step(tok, states, attention)
        out = torch.max(logit, dim=1, keepdim=True)[1]
        return out, states, score

#Encoder-attention-decoder
class Abstract(nn.Module):
    def __init__(self, vocab_size, emb_dim, n_hidden, bidirectional, n_layer, dropout=0):
        super().__init__()
        self._embedding=nn.Embedding(vocab_size, emb_dim, padding_idx=0)
        self._enc_lstm = nn.LSTM(emb_dim, n_hidden, n_layer, bidirectional=bidirectional, dropout=dropout)
        state_layer = n_layer * (2 if bidirectional else 1)
        enc_out_dim = n_hidden* (2 if bidirectional else 1)
        self._init_enc_h = nn.Parameter(torch.Tensor(state_layer, n_hidden))
        self._init_enc_c = nn.Parameter(torch.Tensor(state_layer, n_hidden))
        init.uniform_(self._init_enc_h, -INIT, INIT)
        init.uniform_(self._init_enc_c, -INIT, INIT)
        self._dec_lstm = MultiLayerLSTMCells(2*emb_dim, n_hidden, n_layer, dropout=dropout)
        self._attention_wm = nn.Parameter(torch.Tensor(enc_out_dim, n_hidden))
        self._attention_wq = nn.Parameter(torch.Tensor(n_hidden, n_hidden))
        init.xavier_normal_(self._attention_wm)
        init.xavier_normal_(self._attention_wq)
        self._dec_h = nn.Linear(enc_out_dim, n_hidden, bias=False)
        self._dec_c = nn.Linear(enc_out_dim, n_hidden, bias=False)
        self._projection = nn.Sequential(nn.Linear(2*n_hidden, n_hidden), nn.Tanh(), nn.Linear(n_hidden, emb_dim, bias=False))
        self._copy = _CopyLinear(n_hidden, n_hidden, 2*emb_dim)
        self._decoder = CopyLSTMDecoder(self._copy, self._embedding, self._dec_lstm, self._attention_wq, self._projection)
    def forward(self, article, art_lens, abstract, extend_art, extend_vsize):
        attention, init_dec_states = self.encode(article, art_lens)
        mask = len_mask(art_lens, attention.device).unsqueeze(-2)
        logit = self._decoder((attention, mask, extend_art, extend_vsize),init_dec_states, abstract)
        return logit

    def encode(self, article, art_lens=None):
        size = (self._init_enc_h.size(0), len(art_lens) if art_lens else 1, self._init_enc_h.size(1))
        init_enc_states = (self._init_enc_h.unsqueeze(1).expand(*size), self._init_enc_c.unsqueeze(1).expand(*size))
        enc_art, final_states = lstm_encoder(article, self._enc_lstm, art_lens, init_enc_states, self._embedding)
        #because if its bidirectional h is in of shape = [h-> <-h] into concatinating on 2nd dim
        if self._enc_lstm.bidirectional:
            h,c = final_states
            final_states = (torch.cat(h.chunk(2, dim=0), dim=2), torch.cat(c.chunk(2, dim=0), dim=2))
        init_h =  torch.stack([self._dec_h(s) for s in final_states[0]], dim=0)
        init_c =  torch.stack([self._dec_c(s) for s in final_states[1]], dim=0)
        init_dec_states = (init_h, init_c)
        attention = torch.matmul(enc_art, self._attention_wm).transpose(0,1)
        init_attn_out = self._projection(torch.cat([init_h[-1], sequence_mean(attention, art_lens, dim=1)], dim=1))
        return attention, (init_dec_states, init_attn_out)

    def set_embedding(self, embedding):
        self._embedding.weight.data.copy_(embedding)
    def batch_decode(self, article, art_lens, extend_art, extend_vsize, go, eos, unk, max_len):
        batch_size = len(art_lens)
        vsize = self._embedding.num_embeddings
        attention, init_dec_states = self.encode(article, art_lens)
        mask = len_mask(art_lens, attention.device).unsqueeze(-2)
        attention = (attention, mask, extend_art, extend_vsize)
        tok = torch.LongTensor([go]*batch_size).to(article.device)
        outputs = []
        attns = []
        states = init_dec_states
        for i in range(max_len):
            tok, states, attn_score = self._decoder.decode_step(tok, states, attention)
            attns.append(attn_score)
            outputs.append(tok[:, 0].clone())
            tok.masked_fill_(tok >= vsize, unk)
        return outputs, attn
